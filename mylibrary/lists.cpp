#include<iostream>
#include "list.h"




namespace mylib {


  List::List(std::initializer_list<value_type> list)
    : _size {list.size()}

  {
      head = nullptr;
      last = nullptr;
            for(auto a:list)
            {
                //std::cout<<"\n value = "<<a;
                if(head == nullptr)
                {
                    //list_item * temp = new list_item();

                    std::shared_ptr<list_item> temp = std::make_shared<list_item>();
                    temp->value = a;
                    temp->next = nullptr;
                    head = temp;
                    last = temp;
                    //next = temp;
                }
                else
                {
                    //list_item * temp = new list_item();
                    std::shared_ptr<list_item> temp = std::make_shared<list_item>();
                    temp->value = a;
                    temp->next = nullptr;
                    last->next = temp;
                    last=temp;
                }

            }
             std::cout<<"\n size = "<<_size<<"\n";

   //End


  }

  List::Push_Back(int a)
  //: _size {list.size()}
  {
      //list_item * current = head;
      iterator current = std::make_shared<list_item>();
      current = head;
      if(current == nullptr)
          std::cout<<"\nEmpty LinkList";
      while(current != nullptr)
      {
          current = current->next;
      }
      //list_item * temp = new list_item();
      std::shared_ptr<list_item> temp = std::make_shared<list_item>();
      temp->value = a;
      temp->next = nullptr;
      last->next = temp;
      last=temp;
      std::cout<<"\nElement Inserted at the last Position using Push_back() = "<<a;
      ++_size;

  }

  List::Push_front(int a)
  {
      //list_item * temp = new list_item();
      std::shared_ptr<list_item> temp = std::make_shared<list_item>();
      temp->value = a;
      temp->next = head;
      head = temp;
      std::cout<<"\nElement Inserted at the First Position using Push_front() = "<<a;
      ++_size;
  }

  List::display()
  {
      //list_item * current = head;
      iterator current = std::make_shared<list_item>();
      current = head;
      if(current == nullptr)
          std::cout<<"\nEmpty LinkList";
      else
      {
        std::cout<<"\n\nDisplaying List = ";
        while(current != nullptr)
        {
            std::cout<<current->value<<"  ";
            current = current->next;
        }
        std::cout<<"\n";
      }
      std::cout<<"\n size = "<<_size<<"\n";

  }

  std::shared_ptr<list_item> List::Front()
  {
      return head;
  }

  std::shared_ptr<list_item> List::Back()
  {
      //list_item * current = head;
      iterator current = std::make_shared<list_item>();
      current = head;
      //list_item * previous;
      iterator previous = std::make_shared<list_item>();
      if(current == nullptr)
          std::cout<<"\nEmpty LinkList";
      else
      {
        while(current != nullptr)
        {
            previous = current;
            current = current->next;
        }
        return previous;
      }

  }

  int List::Begin()
  {
      //int i = 0;
      //list_item * current = head;
      iterator current = std::make_shared<list_item>();
      current = head;
      if(current == nullptr)
      {
          std::cout<<"\nList is empty\n";
          return 0;
      }
      else
        return current->value;
  }

  int List::End()
  {
      //int i = 0;
      //list_item * current = head;
      iterator current = std::make_shared<list_item>();
      current = head;
      if(current == nullptr)
      {
          std::cout<<"\nList is empty\n";
          return 0;
      }
      else
      {
        while(current != nullptr)
        {
            current = current->next;
            if(current->next == nullptr)
                break;
        }
        return current->value;
      }

  }


  List::size_type
  List::size() const
  {
      return _size;
  }

  //begin


    /*List::begin(std::initializer_list<value_type> list)

    {
        //list_item* current = &root_item;
        if(current != nullptr)
        {
            std::cout<<"\n"<<"First Value = "<<current->value;
            std::cout<<"\n";
        }
        /*for(auto a:list)
        {
            if(current != nullptr)
            {
                std::cout<<"\n"<<"First Value = "<<a;
                break;
            }
            else
            {
                std::cout<<"\nYour List is Empty\n";
                break;
            }

        }
        for(auto a:list)
        {
            current->value = a;
            if(current->next == nullptr)
            {
                current->next = new list_item();
            }
            current = current->next;
        }
    }*/

    /*List::End( std::initializer_list<value_type> list)
    {

    }

    Front(std::initializer_list<value_type> list)
    {

    }

    Back(std::initializer_list<value_type> list)
    {

    }*/

    /*List::Push_Back(std::initializer_list<value_type> list)
    {
        list_item* current = &root_item;
        for(auto a:list)
        {
            current->value = a;
            if(current->next == nullptr)
            {
                current->next = new list_item();
            }
            current = current->next;
        }
    }*/

  //end



}
        // END namespace mylib


