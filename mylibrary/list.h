#ifndef LIST_H
#define LIST_H


#include <initializer_list>
#include <memory>


namespace mylib {

class list_item
{
    public:
    int val;
    std::shared_ptr<list_item> next;
    list_item(int value = 0) : v(value), next(nullptr) {}

    //void begin(std::intializer_list<value_type> list);
};

  class List {
  public:
    using size_type  = std::size_t;
    using value_type = int;
    using iterator = std::shared_ptr<list_item>;
    using constant_iterator = std::shared_ptr<const list_item>;


    List( std::initializer_list<value_type> list );

    //begin( std::initializer_list<value_type> list );
    size_type size() const;

//Begin checking
    Push_Back(int a);
    Push_front(int a);
    display();
    std::shared_ptr<list_item> Front();
    std::shared_ptr<list_item> Back();
    int Begin();
    int End();
    /*End(std::initializer_list<value_type> list);
    Front(std::initializer_list<value_type> list);
    Back(std::initializer_list<value_type> list);*/
    //Push_Back(std::initializer_list<value_type> list);

    //List(){value = 0;}

//End checking
  private:
    size_type _size;
    iterator head;// = std::make_shared<list_item>();
    //list_item * head = nullptr;
    iterator last;// = std::make_shared<list_item>();
    //list_item * last = nullptr;
  }; // END class List



}  // END namespace mylib

#endif // LIST_H
