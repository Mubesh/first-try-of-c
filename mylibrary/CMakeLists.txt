# Set a project name an version number for our library
project(mylibrary VERSION 1.0)

# List header files
set(HDRS
  functions.h
  shellsort.h
  vector.h
  list.h

)

# List source files
set(SRCS
  functions.cpp
lists.cpp


)


# Unit testing
if(GTEST_FOUND)

  # Add test
  ADD_TESTS(shellsort_tests)
  ADD_TESTS(vector_tests)

endif(GTEST_FOUND)



# Add rules to create a library
add_library(${PROJECT_NAME} SHARED ${SRCS} ${HDRS})


# Tell the compiler to use the our compile flags
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS ${MY_COMPILE_FLAGS})

